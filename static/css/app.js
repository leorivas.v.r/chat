function ocultar(){
    document.getElementById('uno').style.display='none'
}
function mostrar(){
    document.getElementById('uno').style.display=null
}
function borrarDoc(){
    document.getElementById('doc').style.display=null
}
class Chatbox {
    constructor() {
        this.args = {
            
            openButton: document.querySelector('.chatbox__button'),
            cerrarButton:document.querySelector('.cerra'),
            chatBox: document.querySelector('.chatbox__support'),
            sendButton: document.querySelector('.send__button'),
            openButton1: document.querySelector('.boton__articulos'),
            openButton2:document.querySelector('.cerrar_encabezado'),
            tex: document.querySelector('.articulos'),
        }

        this.state = false;
        this.messages = [];
    }

    display() {
        const {openButton2, tex,openButton1,cerrarButton,openButton, chatBox, sendButton } = this.args;
        openButton1.addEventListener('click', () => this.toggleState(tex))
        openButton2.addEventListener('click', () => this.toggleState(tex))
        openButton.addEventListener('click', () => this.toggleState(chatBox))
        cerrarButton.addEventListener('click', () => this.toggleState(chatBox))
        sendButton.addEventListener('click', () => this.onSendButton(chatBox))
        

        const node = chatBox.querySelector('input');
        node.addEventListener("keyup", ({ key }) => {
            if (key === "Enter") {
                this.onSendButton(chatBox)
            }
        })
    }

    toggleState(chatbox) {
        this.state = !this.state;
        // permite ocultar o mostra el chabot cuado se da clik sobre el icono principal
        if (this.state) {
            chatbox.classList.add('chatbox--active')
           
        } else {
            chatbox.classList.remove('chatbox--active')
        }
    }
    toggleState(chatbox1) {
        this.state = !this.state;
        // permite ocultar o mostra el chabot cuado se da clik sobre el icono principal
        if (this.state) {
            chatbox1.classList.add('articulos--active')
           
        } else {
            chatbox1.classList.remove('articulos--active')
        }
    }
    //proceso de interacion entre el chatbot, el usuario y la red neuronal
    onSendButton(chatbox) {
        //permite acceder a la caja de texto donde el usuario hace la pregunta
        var textField = chatbox.querySelector('input');
        //extrae el valor(pregunta) ingresada por el usuario
        let text1 = textField.value
        if (text1 === "") {
            return;
        }
        //se identifica que la persona que ingrasa la pregunta es el ususario
        let msg1 = { name: "Usuario", message: text1 }
        this.messages.push(msg1);
        //se envia la pregunta con la solicitud POST
        fetch('http://127.0.0.1:5050/predict', {
            method: 'POST',
            body: JSON.stringify({ message: text1 }),
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
        }) // Se envita la respuesta que predijo el moledo a la interfaz
            .then(r => r.json())
            .then(r => {
                //se identifica que sea el chabot quien respoda
                let msg2 = { name: "UNL", message:r.answer };
                this.messages.push(msg2);
                this.updateChatText(chatbox)
                textField.value = ''
            
            }).catch((error) => {
                console.error('Error:', error);
                this.updateChatText(chatbox)
                textField.value = ''
            });
    }
    //función que permite que los mensaje aparescan de forma contigua sin que se borren
    //cada vez que el usuario haga una pregunta
    updateChatText(chatbox) {
        var html = '';
        this.messages.slice().reverse().forEach(function (item, index) {
        
            //respueta realizada por el chat
            if (item.name === "UNL") {
               var cadena="No he podido encontrar información para su pregunta."
               var dato=item.message
               var datos=dato.split("_")
               if(datos[0]==cadena ){
                    html += '<div class="messages__item messages__item--visitor">' + cadena+" A continuación se le presentan algunas sugerencias que puede escribir: "+
                    '<FONT color="blue">'+"Calendario Académico, Formas de hologación, Requisitos de ingreso, Coste de un posgrado"+'</FONT>' +'</div>'
               }
               if (datos[1]!=undefined && datos[0]!=cadena ){ 
                var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig; 
                html += '<div class="messages__item messages__item--visitor">' + datos[0].replace(exp,
                "<a href='$1'>$1</a>") +". "+'<FONT SIZE="1.9" color="Blue">'+'<b>'+datos[1]+'</b>'+'</FONT>'+'</div>'
            
                    
                }if(datos[0]!=cadena && datos[1]==undefined){
                    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig; 
                    html += '<div class="messages__item messages__item--visitor">' + datos[0].replace(exp,
                    "<a href='$1'>$1</a>") +'</div>'
                    }
            }
            //pregunta realizada por el usuario
            else {
                html += '<div class="messages__item messages__item--operator">' + item.message +'</div>'
            }
        });

        const chatmessage = chatbox.querySelector('.chatbox__messages');
        chatmessage.innerHTML = html;
    }
}


const chatbox = new Chatbox();
chatbox.display();